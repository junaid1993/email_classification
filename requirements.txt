Flask==0.12.2
google-cloud-vision==0.30.1
google-cloud-storage==1.7.0
pandas==0.22.0
scipy==1.0.0
Werkzeug
six==1.11.0
chardet==3.0.4
nltk==3.2.5
requests
tabulate
scikit-learn
h2o==3.18.0.11
xlrd==1.1.0
keras==2.1.3
tensorflow==1.4.0
h5py==2.7.1