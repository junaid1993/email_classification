from flask import Flask, render_template, Response, request
from google.cloud import storage
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
from werkzeug import secure_filename
import h2o
import pickle
from nltk import word_tokenize
import os
import time
import six
import chardet

from keras.models import Model
from keras.layers import Embedding,Conv1D,MaxPool1D,Input,Concatenate,GlobalAveragePooling1D
from keras.layers.core import Dense,Dropout
from keras.preprocessing import sequence
import pickle
import gzip

import re

# PreProcessing 
def pre_process(s):
	# 1. convert to lower
	s = s.lower()

	# Remove pipe sign, forward and back slashes
	PATTERN = r"[\||\\|//]"
	s = re.sub(PATTERN, r' ', s)

	# Remove some un-wanted characters
	PATTERN = r"[\(+|\)+|_+|<+|>+|\[+|\]+|\-+]|re|subject|subject:|subject]|@"
	s = re.sub(PATTERN, r'', s)

	# Replace annonymized information
	PATTERN = r"xx+"
	s = re.sub(PATTERN, r'annonymized_mask_123', s)

	# End of the sentences this will probably show us how many number of sentences are there 
	PATTERN = r"\."
	s = re.sub(PATTERN, r' . ', s)

	PATTERN = r","
	s = re.sub(PATTERN, r' , ', s)

	PATTERN = r":"
	s = re.sub(PATTERN, r' : ', s)

	PATTERN = r";"
	s = re.sub(PATTERN, r' ; ', s)

	PATTERN = r"\$"
	s = re.sub(PATTERN, r' $ ', s)

	PATTERN = r"&"
	s = re.sub(PATTERN, r' & ', s)

	return s

def load_zipped_pickle(filename):
	with gzip.open(filename, 'rb') as f:
		loaded_object = pickle.load(f)
		return loaded_object
app = Flask(__name__)

class LemmaTokenizer(object):
  def __init__(self):
    self.wnl = WordNetLemmatizer()
  def __call__(self, doc):
    return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]

def upload_file_to_bucket(path, filename):
	client = storage.Client(project = "singtel-email-poc")
	bucket = client.bucket("signtel_email_uploads")
	blob   = bucket.blob(filename + "_" + str(time.time()) + ".csv")
	blob.upload_from_filename(path)
	blob.make_public()

	url = blob.public_url

	if isinstance(url, six.binary_type):
		url = url.decode('utf-8')

	return url

def read_dataset(file, format):
	#Saving uploaded file
	myfile = request.files['myfile']
	myfile.save(secure_filename(myfile.filename))
	
	# Loading file to process
	if format == "excel":
		df = pd.read_excel(myfile.filename, dtype=str)
	elif format == "csv":
		df = pd.read_csv(myfile.filename, dtype=str)
	df = df.fillna('')
	os.remove(myfile.filename)
	return df

@app.route("/")
def hello():
  return render_template('best_model.html')

@app.route("/all_models")
def all_models():
  return render_template('all_models.html')

@app.route("/process", methods=['POST'])
def process():
	if request.method == 'POST' and request.files['myfile']:

		test = read_dataset(request.files['myfile'], request.form.get("file_format"))
		# test_1 = test[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO"]]
		test["CHN_MESSAGE_MASKED"] = test["CHN_MESSAGE_MASKED"].apply(lambda x: pre_process(x))

		x_test = test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
		X_test_counts = count_vect.transform(x_test)
		# print("going to convert dataframe to h2o frame")
		# test_1 = h2o.H2OFrame(test_1)
		testing = h2o.H2OFrame(X_test_counts)
		predictions = se.predict(testing)
		# predictions = glm.predict(testing)

		test["a_predictions"] = predictions["predict"].as_data_frame(use_pandas=True)["predict"]
		test.to_csv("results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./results.csv","results")
		os.remove("results.csv")
		return Response(url)
 
@app.route("/advanced", methods=['POST'])
def advance_process():
	if request.method == 'POST' and request.files['myfile']:
		test = read_dataset(request.files['myfile'], request.form.get("file_format"))
		filtered_test = test[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO"]]

		# apply filtering, because we will predict only first email and assign 
		filtered_test = filtered_test.loc[filtered_test['CHN_NO']=="0"]
		filtered_test = filtered_test.drop_duplicates(subset='Ticket#', keep='first', inplace=False)
		filtered_test["CHN_MESSAGE_MASKED"] = filtered_test["CHN_MESSAGE_MASKED"].apply(lambda x: pre_process(x))
		
		# Getting X_tests and generating cout vectors
		x_test = filtered_test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
		X_test_counts = count_vect.transform(x_test)
		
		testing = h2o.H2OFrame(X_test_counts)
		predictions = se.predict(testing)
		# predictions = glm.predict(testing)
		filtered_test["predictions"] = predictions["predict"].as_data_frame(use_pandas=True)["predict"]
		
		test["a_predictions"] = "blank"
		for index, row in filtered_test.iterrows():
			test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'] = row["predictions"]

		# predictions_df =  predictions["predict"].as_data_frame(use_pandas=True)["predict"]
		# test["a_predictions"] = predictions_df
		test.to_csv("Results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./Results.csv","Results")
		os.remove("Results.csv")
		return Response(url)

@app.route("/majority_vote", methods=['POST'])
def majority_vote():
	if request.method == 'POST' and request.files['myfile']:
		#Saving uploaded file
		# myfile = request.files['myfile']
		# myfile.save(secure_filename(myfile.filename))

		# Loading file to process
		# test = pd.read_excel(myfile.filename, dtype=str)
		test = read_dataset(request.files['myfile'], request.form.get("file_format"))
		# test_1 = test[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO"]]
		test["CHN_MESSAGE_MASKED"] = test["CHN_MESSAGE_MASKED"].apply(lambda x: pre_process(x))

		x_test = test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
		X_test_counts = count_vect.transform(x_test)
		# print("going to convert dataframe to h2o frame")
		# test_1 = h2o.H2OFrame(test_1)
		testing = h2o.H2OFrame(X_test_counts)
		predictions = se.predict(testing)
		# predictions = glm.predict(testing)
		test["a_predictions"] = predictions["predict"].as_data_frame(use_pandas=True)["predict"]
		
		processed_tickets = []
		for index, row in test.iterrows():
			if row["Ticket#"] in processed_tickets:
				continue;
			test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'] = test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'].value_counts().idxmax()
			processed_tickets.append(row["Ticket#"])
		test.to_csv("results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./results.csv","results")
		os.remove("results.csv")
		return Response(url)

@app.route("/deep_learning_simple", methods=['POST'])
def deep_learning():
	if request.method == 'POST' and request.files['myfile']:
		test = read_dataset(request.files['myfile'], request.form.get("file_format"))

		# Getting X_tests and generating cout vectors		
		x_test = test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
	
		maxWordCount = 100
		encoded_words3 = Tokenizer.texts_to_sequences(x_test)
		X_test_encodedPadded_words = sequence.pad_sequences(encoded_words3, maxlen=maxWordCount)

		preds = model.predict(X_test_encodedPadded_words)
		predicted = lb.inverse_transform(preds)

		test["a_predictions"] = pd.Series(predicted)
		
		test.to_csv("results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./results.csv","results")
		os.remove("results.csv")
		return Response(url)

@app.route("/deep_learning_first_email", methods=['POST'])
def deep_learning_first_email():
	if request.method == 'POST' and request.files['myfile']:
		test = read_dataset(request.files['myfile'], request.form.get("file_format"))
		# test_1 = test[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO"]]
		filtered_test = test[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO"]]

		# apply filtering, because we will predict only first email and assign 
		filtered_test = filtered_test.loc[filtered_test['CHN_NO']=="0"]
		filtered_test = filtered_test.drop_duplicates(subset='Ticket#', keep='first', inplace=False)
		filtered_test["CHN_MESSAGE_MASKED"] = filtered_test["CHN_MESSAGE_MASKED"].apply(lambda x: pre_process(x))
		
		# Getting X_tests and generating cout vectors		
		x_test = filtered_test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
	
		maxWordCount = 100
		encoded_words3 = Tokenizer.texts_to_sequences(x_test)
		X_test_encodedPadded_words = sequence.pad_sequences(encoded_words3, maxlen=maxWordCount)

		preds = model.predict(X_test_encodedPadded_words)
		predicted = lb.inverse_transform(preds)

		filtered_test["predictions"] = predicted
		
		test["a_predictions"] = "blank"
		for index, row in filtered_test.iterrows():
			test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'] = row["predictions"]
		
		test.to_csv("results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./results.csv","results")
		os.remove("results.csv")
		return Response(url)

@app.route("/deep_learning_majority_vote", methods=['POST'])
def deep_learning_majority_vote():
	if request.method == 'POST' and request.files['myfile']:
		test = read_dataset(request.files['myfile'], request.form.get("file_format"))
		
		# Getting X_tests and generating cout vectors		
		x_test = test["CHN_MESSAGE_MASKED"].values.astype('U').tolist()
	
		maxWordCount = 100
		encoded_words3 = Tokenizer.texts_to_sequences(x_test)
		X_test_encodedPadded_words = sequence.pad_sequences(encoded_words3, maxlen=maxWordCount)

		preds = model.predict(X_test_encodedPadded_words)
		predicted = lb.inverse_transform(preds)
		
		test["a_predictions"] = pd.Series(predicted)
		
		processed_tickets = []
		for index, row in test.iterrows():
			if row["Ticket#"] in processed_tickets:
				continue;
			test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'] = test.ix[test["Ticket#"] == row["Ticket#"], 'a_predictions'].value_counts().idxmax()
			processed_tickets.append(row["Ticket#"])

		test.to_csv("results.csv", index=False)
		# h2o.export_file(test, myfile.filename, force = True)
		url = upload_file_to_bucket("./results.csv","results")
		os.remove("results.csv")
		return Response(url)

if __name__ == "__main__":
	h2o.init()
	# h2o.connect()
	count_vect = pickle.load( open( "vectorizer.pk", "rb" ) )
	se = h2o.load_model('StackedEnsemble_AllModels_0_AutoML_20180701_134145')
	glm = h2o.load_model('GLM_grid_0_AutoML_20180701_134145_model_0')

	Tokenizer = load_zipped_pickle('Tokenizer.pkl')
	Tokenizer_vocab_size = len(Tokenizer.word_index) + 1
	lb = load_zipped_pickle('label_binarizer.pkl')

	# Some Parameters
	sequence_length = 100 # 56
	vocabulary_size = Tokenizer_vocab_size
	embedding_dim = 256
	filter_sizes = [3,4,5]
	num_filters = 512
	drop = 0.5

	print("-------------- Creating Model ---------------")
	inputs = Input(shape=(sequence_length,), dtype='int32')
	embedding = Embedding(input_dim=vocabulary_size, output_dim=embedding_dim, input_length=sequence_length)(inputs)
	embedding = Dropout(0.25)(embedding)

	conv_0 = Conv1D(num_filters, kernel_size=(filter_sizes[0]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)
	conv_1 = Conv1D(num_filters, kernel_size=(filter_sizes[1]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)
	conv_2 = Conv1D(num_filters, kernel_size=(filter_sizes[2]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)


	maxpool_0 = GlobalAveragePooling1D()(conv_0)
	maxpool_1 = GlobalAveragePooling1D()(conv_1)
	maxpool_2 = GlobalAveragePooling1D()(conv_2)


	concatenated_tensor = Concatenate()([maxpool_0, maxpool_1, maxpool_2])
	dropout_1 = Dropout(drop)(concatenated_tensor)
	output = Dense(units=9, activation='softmax')(dropout_1)

	# this creates a model that includes
	model = Model(inputs=inputs, outputs=output)

	model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])
	model.load_weights('10-17-1.218-0.603.hdf5')

	print("Going to start application")
	app.run(host = '0.0.0.0',port=3000)
	print("App Running")




