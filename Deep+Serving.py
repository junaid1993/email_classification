
# coding: utf-8

# In[1]:


#importing libraries
#reading data from files
import pandas as pd


# In[2]:


#helper functions
import re
# PreProcessing 
def pre_process(s):
    # 1. convert to lower
    s = s.lower()
    
    # Remove pipe sign, forward and back slashes
    PATTERN = r'[\||\\|//]'
    s = re.sub(PATTERN, r' ', s)
    
    # Remove some un-wanted characters
    PATTERN = r'[\(+|\)+|_+|<+|>+|\[+|\]+|\-+]|re|subject|subject:|subject]|@'
    s = re.sub(PATTERN, r'', s)

    # Replace annonymized information
    PATTERN = r'xx+'
    s = re.sub(PATTERN, r'annonymized_mask_123', s)
    
    # End of the sentences this will probably show us how many number of sentences are there 
    PATTERN = r'\.'
    s = re.sub(PATTERN, r' . ', s)
    
    PATTERN = r','
    s = re.sub(PATTERN, r' , ', s)
    
    PATTERN = r':'
    s = re.sub(PATTERN, r' : ', s)
    
    PATTERN = r';'
    s = re.sub(PATTERN, r' ; ', s)
    
    PATTERN = r'\$'
    s = re.sub(PATTERN, r' $ ', s)
    
    PATTERN = r'&'
    s = re.sub(PATTERN, r' & ', s)

    return s


# Save a dictionary into a pickle file.
import pickle
import gzip

#reference:https://stackoverflow.com/questions/18474791/decreasing-the-size-of-cpickle-objects
#Used for loading/saving the objects with gunzip

def save_zipped_pickle(obj, filename, protocol=-1):
    with gzip.open(filename, 'wb') as f:
        pickle.dump(obj, f, protocol)
        
def load_zipped_pickle(filename):
    with gzip.open(filename, 'rb') as f:
        loaded_object = pickle.load(f)
        return loaded_object


# In[31]:


#loading files
df=pd.read_excel('data/FS - Feb - 1st to 4th Feb 2018_chains.xlsx',index_col=None, header=0)


# In[32]:


test = df[['CHN_MESSAGE_MASKED', 'Ticket#', "CHN_NO",'Category 1 Name']]


# In[33]:


test["CHN_MESSAGE_MASKED"] = test["CHN_MESSAGE_MASKED"].values.astype('str')
test["Category 1 Name"] = test["Category 1 Name"].values.astype('str')


# In[34]:


test['CHN_MESSAGE_MASKED']=test['CHN_MESSAGE_MASKED'].apply(pre_process)


# In[35]:


old=test





# In[38]:


x_test=test['CHN_MESSAGE_MASKED']
y_test=test['Category 1 Name']


# In[39]:


from keras.models import Model
from keras.layers import Embedding,Conv1D,MaxPool1D,Input,Concatenate,GlobalAveragePooling1D
from keras.layers.core import Dense,Dropout
from keras.preprocessing import sequence


# In[40]:


Tokenizer = load_zipped_pickle('Tokenizer.pkl')
Tokenizer_vocab_size = len(Tokenizer.word_index) + 1
print ("Vocab size",Tokenizer_vocab_size)


maxWordCount= 100

encoded_words3 = Tokenizer.texts_to_sequences(x_test)


#padding all text to same size
X_test_encodedPadded_words = sequence.pad_sequences(encoded_words3, maxlen=maxWordCount)


# In[41]:


lb = load_zipped_pickle('label_binarizer.pkl')


# In[42]:


sequence_length = X_test_encodedPadded_words.shape[1] # 56
vocabulary_size = Tokenizer_vocab_size
embedding_dim = 256
filter_sizes = [3,4,5]
num_filters = 512
drop = 0.5


# In[43]:


print("Creating Model...")
inputs = Input(shape=(sequence_length,), dtype='int32')
embedding = Embedding(input_dim=vocabulary_size, output_dim=embedding_dim, input_length=sequence_length)(inputs)
embedding = Dropout(0.25)(embedding)

conv_0 = Conv1D(num_filters, kernel_size=(filter_sizes[0]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)
conv_1 = Conv1D(num_filters, kernel_size=(filter_sizes[1]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)
conv_2 = Conv1D(num_filters, kernel_size=(filter_sizes[2]), padding='valid', kernel_initializer='normal', activation='relu')(embedding)


maxpool_0 = GlobalAveragePooling1D()(conv_0)
maxpool_1 = GlobalAveragePooling1D()(conv_1)
maxpool_2 = GlobalAveragePooling1D()(conv_2)


concatenated_tensor = Concatenate()([maxpool_0, maxpool_1, maxpool_2])
dropout_1 = Dropout(drop)(concatenated_tensor)
output = Dense(units=9, activation='softmax')(dropout_1)

# this creates a model that includes
model = Model(inputs=inputs, outputs=output)

model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])


# In[44]:


model.load_weights('10-17-1.218-0.603.hdf5')


# In[45]:


preds = model.predict(X_test_encodedPadded_words)


# In[46]:


predicted=lb.inverse_transform(preds)


# In[47]:


print(classification_report(y_test, predicted, digits=3))